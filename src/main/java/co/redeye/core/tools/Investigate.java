package co.redeye.core.tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Investigate {

    //private static final String filePath = "/media/george/Seagate Backup Plus Drive/QUU First Set 150915";
    private static final String filePath = "/home/george/Downloads/Example Alacer Documents";

    static void usage() {
        System.err.println("java Investigate <path>");
        System.exit(-1);
    }

    public static void main(String[] args)
        throws IOException {

        Path startingDir = Paths.get(filePath);
        if (args.length > 1) {
            usage();
        } else if (args.length == 1) {
            startingDir = Paths.get(args[0]);
        }

        FileStalker stalker = new FileStalker();
        Files.walkFileTree(startingDir, stalker);
        stalker.done();
    }
}
