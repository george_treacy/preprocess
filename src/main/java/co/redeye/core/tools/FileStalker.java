package co.redeye.core.tools;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.parser.DocumentScrape;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import org.apache.tika.metadata.Metadata;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by george on 23/09/15.
 */
public class FileStalker extends SimpleFileVisitor<Path> {

    private static final Logger logger = LoggerFactory.getLogger(FileStalker.class);
    private static final String tableName = "FilePreProcess";


    private DynamoDB dynamo;
    private Table table;
    private AmazonDynamoDBClient client;

    private DocumentScrape scrape;

    FileStalker() {
        init();
        table = dynamo.getTable(tableName);
        scrape = new DocumentScrape();
    }

    private void init() {
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
        client = new AmazonDynamoDBClient(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        client.setRegion(usWest2);
        dynamo = new DynamoDB(client);
    }

    void getNextFile(Path file) {
        Path name = file.getFileName();
        if (name != null) {
            System.out.println(file);
        }
        process(file);
    }

    private void process(final Path file) {
        scrape.resetParser();
        final String absolutePath = file.toAbsolutePath().toString();
        try {
            scrape.tikaAutoParse(absolutePath);
        } catch (DocumentProcessorException e) {
            e.printStackTrace();
        }

        // Create a json object and persist it in the cloud.

        final String hashKey = getSha(absolutePath);
        logger.debug("Hash Key: " + hashKey);

        // don't forget to persis the absolute path.

        // get the file size.
        final long fileSize = new File(absolutePath).length();
        logger.debug("File Size Bytes:  " + fileSize);

        // BodyText will be limited to max 100,000 chars by tika exception.
        // If the bodyText is less than 100 chars, it is probably a scan etc...
        // even if it's file type is pdf.
        String bodyText = scrape.getHandler().toString();
        logger.debug("Body Text: " + bodyText.substring(0, Math.min(bodyText.length(), 100)));
        if (bodyText.length() == 0) {
            bodyText = "empty";
        }

        // The metadata keys.
        Map<String, String> attributes = getAttributes(scrape.getMetadata());
        logger.debug("Attributes: " + attributes.toString() + "\n\n");


        doDynamo(hashKey, bodyText, fileSize, absolutePath, attributes);

    }

    private void doDynamo(final String hashKey, final String bodyText, final long fileSize, final String absolutePath, final Map<String, String> attributes) {
        // Add another awesome item
        Item item = new Item()
                        .withPrimaryKey("name", hashKey)
                        .withString("absolutePath", absolutePath)
                        .withLong("fileSize", fileSize);

        for (String key : attributes.keySet()) {
            final String value = attributes.get(key);
            if (value != null && !"".equals(value))
                item.withString(key, attributes.get(key));
        }

        item.withString("bodyText", bodyText);

        table.putItem(item);
    }

    private Map<String, String> getAttributes(final Metadata metadata) {
        Map<String, String> attributes = new HashMap<>();
        for (String key : metadata.names()) {
            attributes.put(key, metadata.get(key));
        }
        return attributes;
    }

    private String getSha(final String text) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            md.update(text.getBytes("UTF-8")); // or UTF-16 if needed
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        return new String(Hex.encode(digest));
    }

    void done() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(tableName);

        ScanResult result = client.scan(scanRequest);
        for (Map<String, AttributeValue> item : result.getItems()){
            printItem(item);
        }
        logger.debug("Done");
    }

    void printItem(final Map<String, AttributeValue> item) {
        for (String key : item.keySet()) {
            if (!"bodyText".equals(key))
                logger.debug(key + " " + item.get(key));
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        getNextFile(file);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }
}